const express = require('express');
const http = require('http');
const socketio = require('socket.io');
const morgan = require('morgan');
const routes = require('./routes');

const app = express();
module.exports = app;

const server = http.createServer(app);
const io = socketio.listen(server);

app.set('port', process.env.PORT || 4000);

if (app.get('env') === 'development') {
  app.use(morgan());
}

app.get('/', routes.index)

const chat = io.of('/chat');
chat.on('connection', require('./routes/chat'));

server.listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});
