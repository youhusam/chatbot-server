const _ = require('lodash');
const Fuse = require('fuse.js');

/**
 * Singleton class of the our "smart" chatbot.
 */
class Chatbot {
  static instance = null;

  /**
   * Our predefined replies.
   * @type {array}
   */
  static questions = require('./questions.json');

  // Predefined greetings for when a new user joins.
  greetingMessages = ['Welcome our newest user.', 'A new user has joined, let\'s welcome them.', 'Hello, new user.'];

  // An unknown question for analysis.
  previousQuestion = null;

  constructor() {
    if (!Chatbot.instance) {
      Chatbot.instance = this;
      this.fuse = new Fuse(Chatbot.questions, {
        shouldSort: true,
        includeScore: false,
        threshold: 0.25,
        location: 0,
        distance: 100,
        maxPatternLength: 100,
        minMatchCharLength: 1,
        keys: [
          "q"
        ]
      });
    }

    return Chatbot.instance;
  }

  /**
   * Greet a user with a random message from the greetings array.
   */
  greet() {
    const index = _.random(0, this.greetingMessages.length - 1);
    return this.greetingMessages[index];
  }

  /**
   * Look for a reply to user messges or add new questions to the replies object.
   * @param {string} message The message coming from the user.
   */
  reply(message) {
    const isQuestion = message.endsWith('?');

    // TODO: set is question if ends with questionmark

    const botReplies = this.fuse.search(message);

    if (botReplies.length) {
      this.previousQuestion = null;
      const botReply = botReplies[0];
      const randomIndex = _.random(0, botReply.a.length - 1);
      return botReply.a[randomIndex];
    } else if (isQuestion) {
      this.previousQuestion = message;
    } else if (!isQuestion && this.previousQuestion) {
      // Add an answer to an unkown question.
      Chatbot.questions.push({
        q: this.previousQuestion,
        a: [message]
      });
      this.previousQuestion = null;
    }
  }
}

const instance = new Chatbot();

module.exports = instance;
