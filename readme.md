# Chatbot Server
You know the drill, `npm install`, `npm start`

The bot can reply to the following commands (case-insensitive):
* How are you?
* What is your name?
* What is love?
* Tell me a joke
* What is the area of the USA?
* What is the area of Israel?
* What is the capital city of Australia?

The bot does a fuzzy search within a list of questions, and finds the closest answer.

To add new answers, ask a question ending with a question mark, and the next reply without the question mark will be added as an answer.

eg:
```
user1: What's up?
<bot waiting for answer in background>
user2: The sky.
-----------------
user1: What's up?
chatbot: The sky.
```

If a question is asked after an unknown question is asked, the old question will be unshelved and
 the new question will either be answered if it has an answer or shelved for a new reply.
