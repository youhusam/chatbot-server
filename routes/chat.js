const chatbot = require('../chatbot');
const UIDGenerator = require('uid-generator');
const uidgen = new UIDGenerator();

const chat = async socket => {
  const id = await uidgen.generate();
  socket.emit('assign:id', {
    id
  });

  socket.broadcast.emit('send:message', { user: 'chatbot', message: chatbot.greet() });

  socket.emit("send:message", {
    user: "chatbot",
    message: 'Welcome to our humble chat.'
  });


  // broadcast a user's message to all users
  socket.on('send:message', function (data) {
    if (!data.message) {
      console.error('No Message');
      return;
    }
    const botMessage = chatbot.reply(data.message);
    socket.server.of('/chat').emit('send:message', { user: data.user, message: data.message });
    if (botMessage) {
      socket.server.of('/chat').emit('send:message', { user: 'chatbot', message: botMessage });
    }
  });
}

module.exports = chat;
